package com.example.localizationapp.util

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View

import com.applab.localization.LocalizedApp.Companion.LOCALE_EN
import com.applab.localization.LocalizedApp.Companion.getString
import com.applab.localization.LocalizedApp.Companion.locale
import com.applab.localization.select

import java.text.SimpleDateFormat
import java.util.*

fun View.viewVisibility(visible: Boolean? = false) {
    visibility = if (visible == true) View.VISIBLE else View.GONE
}

fun View.viewInvisibility(visible: Boolean? = false) {
    visibility = if (visible == true) View.VISIBLE else View.INVISIBLE
}

fun <T> choose(condition: Boolean? = false, vt: T, vf: T): T = if (condition == true) vt else vf
fun <T> chooseL(vt: T, vf: T, condition: Boolean? = locale == LOCALE_EN): T =
    if (condition == true) vt else vf

fun Activity.intentActionDial(phone: String) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$phone")
    startActivity(intent)
}

fun Activity.intentActionInstagram(instagramUrl: String) {
    val uri = Uri.parse(instagramUrl)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    intent.setPackage("com.instagram.android")
    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        try {
            intent.setPackage("com.instagram.lite")
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(instagramUrl)))
        }
    }
}



