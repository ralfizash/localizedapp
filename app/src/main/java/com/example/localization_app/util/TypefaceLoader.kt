package com.applab.qsl.util

import android.content.Context
import android.graphics.Typeface
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.applab.localization.LocalizedApp
import com.example.localization_app.R



object TypefaceLoader {
    private lateinit var englishRegularFont: Typeface
    private lateinit var englishMediumFont: Typeface
    private lateinit var englishLightFont: Typeface
    private lateinit var englishBoldFont: Typeface

    private lateinit var arabicRegularFont: Typeface
    private lateinit var arabicMediumFont: Typeface
    private lateinit var arabicLightFont: Typeface
    private lateinit var arabicBoldFont: Typeface

    fun initialize(context: Context) {
        englishRegularFont = ResourcesCompat.getFont(context, R.font.jotia_regular)!!
        englishMediumFont = ResourcesCompat.getFont(context, R.font.jotia_medium)!!
        englishLightFont = ResourcesCompat.getFont(context, R.font.jotia_light)!!
        englishBoldFont = ResourcesCompat.getFont(context, R.font.jotia_bold)!!

        arabicRegularFont = ResourcesCompat.getFont(context, R.font.cairo_regular)!!
        arabicMediumFont = ResourcesCompat.getFont(context, R.font.cairo_medium)!!
        arabicLightFont = ResourcesCompat.getFont(context, R.font.cairo_light)!!
        arabicBoldFont = ResourcesCompat.getFont(context, R.font.cairo_bold)!!
    }

    fun getRegularFont(locale: String?) = when (locale) {
        LocalizedApp.LOCALE_EN -> englishRegularFont
        LocalizedApp.LOCALE_AR -> arabicRegularFont
        else -> error("Unknown locale: $locale")
    }

    fun getMediumFont(locale: String?) = when (locale) {
        LocalizedApp.LOCALE_EN -> englishMediumFont
        LocalizedApp.LOCALE_AR -> arabicMediumFont
        else -> error("Unknown locale: $locale")
    }

    fun getLightFont(locale: String?) = when (locale) {
        LocalizedApp.LOCALE_EN -> englishLightFont
        LocalizedApp.LOCALE_AR -> arabicLightFont
        else -> error("Unknown locale: $locale")
    }

    fun getBoldFont(locale: String?) = when (locale) {
        LocalizedApp.LOCALE_EN -> englishBoldFont
        LocalizedApp.LOCALE_AR -> arabicBoldFont
        else -> error("Unknown locale: $locale")
    }

    fun TextView.setRegularFont(locale: String?) {
        typeface = when (locale) {
            LocalizedApp.LOCALE_EN -> englishRegularFont
            LocalizedApp.LOCALE_AR -> arabicRegularFont
            else -> error("Unknown locale: $locale")
        }
    }

    fun TextView.setMediumFont(locale: String?) {
        typeface = when (locale) {
            LocalizedApp.LOCALE_EN -> englishMediumFont
            LocalizedApp.LOCALE_AR -> arabicMediumFont
            else -> error("Unknown locale: $locale")
        }
    }

    fun TextView.setLightFont(locale: String?) {
        typeface = when (locale) {
            LocalizedApp.LOCALE_EN -> englishLightFont
            LocalizedApp.LOCALE_AR -> arabicLightFont
            else -> error("Unknown locale: $locale")
        }
    }

    fun TextView.setBoldFont(locale: String?) {
        typeface = when (locale) {
            LocalizedApp.LOCALE_EN -> englishBoldFont
            LocalizedApp.LOCALE_AR -> arabicBoldFont
            else -> error("Unknown locale: $locale")
        }
    }

    fun ArrayList<TextView>.setRegularFont(locale: String?) {
        forEach { it.setRegularFont(locale) }
    }

    fun ArrayList<TextView>.setMediumFont(locale: String?) {
        forEach { it.setMediumFont(locale) }
    }

    fun ArrayList<TextView>.setLightFont(locale: String?) {
        forEach { it.setLightFont(locale) }
    }

    fun ArrayList<TextView>.setBoldFont(locale: String?) {
        forEach { it.setBoldFont(locale) }
    }
}