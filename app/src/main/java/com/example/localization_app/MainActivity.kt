package com.example.localization_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.applab.localization.LocalizedApp
import com.applab.qsl.util.TypefaceLoader
import com.example.localization_app.databinding.ActivityMainBinding
import com.example.localizationapp.util.chooseL

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private val localizationDelegate = LocalizationDelegate()

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        LocalizedApp.localeLiveData.observe(this, localizationDelegate)

        binding.switch1?.setOnCheckedChangeListener({ _ , isChecked ->

            if (isChecked) {
                LocalizedApp.updateLocale(chooseL(LocalizedApp.LOCALE_AR, LocalizedApp.LOCALE_EN))
            } else {
                LocalizedApp.updateLocale(chooseL(LocalizedApp.LOCALE_AR, LocalizedApp.LOCALE_EN))
            }
        })

    }

    private inner class LocalizationDelegate() : Observer<String> {
        override fun onChanged(locale: String) {
            val layoutDirection = LocalizedApp.getLayoutDirection(locale)
            val resources = LocalizedApp.getResources(locale)
            binding.apply {
                root.layoutDirection = layoutDirection
                etName.layoutDirection=layoutDirection

                head.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    text = resources.getString(R.string.title)
                }

                etName.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    hint = resources.getString(R.string.name)

                }

                etMobile.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    hint = resources.getString(R.string.mobile)
                }

                etEmail.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    hint= resources.getString(R.string.email)
                }

                etAddress.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    hint= resources.getString(R.string.address)
                }

                btnSubmit.apply {
                    typeface = TypefaceLoader.getBoldFont(locale)
                    text= resources.getString(R.string.submit)
                }


            }
        }

    }
}