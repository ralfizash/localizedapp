package com.example.localization_app

import com.applab.localization.LocalizedApp
import com.applab.qsl.util.TypefaceLoader

class Demo: LocalizedApp() {

    override fun onCreate() {
        super.onCreate()
        TypefaceLoader.initialize(this)
    }

}
