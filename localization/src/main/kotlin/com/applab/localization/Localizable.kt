package com.applab.localization

interface Localizable<T> {
    fun getLocalizedValue(locale: String?): T?
}